def usage(interface):
	with open('/proc/net/dev') as netUsageLogFile:
		netUsage = netUsageLogFile.readlines()

	if(interface == "ethernet"):
		index=2
	if(interface == "wifi"):
		index=6

	interface = netUsage[index]

	DOWN 	= int(interface.split()[1])
	UP 		= int(interface.split()[9])  

	DOWN  = (DOWN/1024.0)/1024.0 #in MegaBytes
	UP    = (UP/1024.0)/1024.0 #in MegaBytes
	return (DOWN, UP)  

def MBorGB(data):
	if(data>1024):
		data = data/1024.0
		data = round(data,2)
		return str(data)+" GB"
	data = round(data,2)	
	return str(data)+" MB";
		
def printUSE(interface, down, up):
	print(interface+" Usage\t:\t",MBorGB(down),"\t",
								MBorGB(up),"\t",
								MBorGB(down+up),"\n",
								)


if __name__ == '__main__':
	edown,eup = usage("ethernet")
	wdown,wup = usage("wifi")

	dash=70
	print("-"*dash)
	print("INTERNET USAGE\t \tDownload\tUpload\t\tTotal")
	print("-"*dash + "\n")
	printUSE("ethernet",edown,eup)
	printUSE("wifi",wdown,wup)
	print("-"*dash)
