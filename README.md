# README #

This repo contains all the scripts that might be useful.

### What is this repository for? ###

* scripts to make life easier


### How do I get set up? ###

* clone the repo
* run `python3 scriptname.py`


###1. Internet Usage Ouput ###

```
#!python

----------------------------------------------------------------------
INTERNET USAGE	 	Download	Upload		Total
----------------------------------------------------------------------

ethernet Usage	:	 61.95 GB 	 18.05 GB 	 80.00 GB 

wifi Usage      :	 10.24 MB 	 41.3 MB 	 51.54 MB 

----------------------------------------------------------------------
```